import numpy as np
import matlab.engine
from nfgParser import getData

filename = raw_input("Provide relative filepath : ")

names, num_actions, u = getData(filename)

n = len(names)
num_actions = np.array(num_actions)
maxi_ind = np.zeros(n)
total_cells = 1

for i in range(n):
    # num_actions[i] = input()
    total_cells = total_cells*num_actions[i]

tmp = 0
for i in range(n):
    tmp += num_actions[i]*(num_actions[i] - 1)


total_cells = int(total_cells)
num_variables = total_cells

#u = np.zeros(total_cells*n)
"""
tmp = int(tmp)
obj = [0.0]*num_variables
cond = (n*tmp + num_variables)*[[float(0.0)]*num_variables]
b = [float(0.0)]*(n*tmp  + num_variables)
cond2 = [[float(0.0)]*num_variables]
b2 = [0.0]
"""
obj = np.zeros(num_variables)
b2 = np.zeros(1)
b = np.zeros( n*tmp  + num_variables)
cond = np.zeros(( n*tmp + num_variables, num_variables))
cond2 = np.zeros((1, num_variables))

# for i in range(total_cells*n):
#     u[i] = input()
u = np.array(u)

def increament(l, index):
    #print "BF", l, index
    l[index] += 1
    it = index
    carry = 0

    while(it != (index - 1 + n) % n):
        l[it] = l[it] + carry
        carry = int(l[it] / num_actions[it])
        l[it] = l[it] % num_actions[it]
        it += 1
        it = it % n

    l[it] = l[it] + carry
    l[it] = l[it] % num_actions[it]
    return l
    #print "INC", l


def convertListToNum(l):
    final_ind = 0
    pro = 1

    for it in range(n):
        final_ind += pro*l[it]
        pro = pro * num_actions[it]

    return final_ind

def FinalconvertListToNum(l):
    final_ind = 0
    pro = 1

    for it in range(n):
        final_ind += pro*maxi_ind[it]
        pro = pro * num_actions[it]

    return final_ind


def convertNumToList(number):
    pro = 1
    ans_l = []
    for i in range(n):
        ans_l[i] = number % num_actions[i]
        number -= ans_l[i]
        number = number / num_actions[i]

    print ans_l


#pl: player, st1: strategy1, st2: strategy2
li = [0]*n

for i in range(num_variables):
    tmp = 0
    for j in range(n):
        tmp += u[i*n + j]
    obj[i] = -tmp

count = 0

for pl in range(n):
    for st1 in range(int(num_actions[pl])):
        for st2 in range(int(num_actions[pl])):
            #print count
            if st1 == st2:
                continue
            num_s_minus_i = total_cells / num_actions[pl]
            li = [0]*n
            for i in range( int(num_s_minus_i) ):
                li[pl] = st2
                second_term_ind = convertListToNum(li)
                second_term = u[ int(second_term_ind*n + pl) ]
                li[pl] = st1
                first_term_ind = convertListToNum(li)
                first_term = u[ int(first_term_ind*n + pl) ]
                #print count, first_term_ind, second_term, first_term
                cond[ count ][ int(first_term_ind) ] = (second_term - first_term)*1.0
                b[ count ] = 0
                li = increament(li, (pl+1)%n)

            count += 1


for i in range(num_variables):
    cond[count][i] = -1
    count += 1
    b[ count ] = 0


for i in range(num_variables):
    cond2[0][i] = float(1)

b2[0] = float(1)


num_conditions = count
"""
for i in range(num_variables):
    print obj[i],

#print "***************"



for i in range(num_conditions):
    for j in range(num_variables):
        cond[i][j] = float(cond[i][j])
        print cond[i][j],

    b[i] = float(b[i])
    print b[i]

for i in range(num_variables):
    print cond2[0][i],

print b2[0]
"""

eng = matlab.engine.start_matlab()
l1 = obj.tolist()
l2 = cond.tolist()
l3 = b.tolist()
#print matlab.double(cond.tolist())
#print matlab.double(obj.tolist()), "\n", matlab.double(cond.tolist()), matlab.double(b.tolist())
ans = eng.linprog(matlab.double(obj.tolist()), matlab.double(cond.tolist()), matlab.double(b.tolist()), matlab.double(cond2.tolist()), matlab.double(b2.tolist()))
#ans = eng.linprog(eng.vec2mat(obj, 1), eng.vec2mat(cond, num_variables), eng.vec2mat(b, 1), eng.vec2mat(cond2, num_variables), eng.vec2mat(b2, 1))

l = [0]*n
print ans
eng.quit()
